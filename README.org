* Instalar Qt en facil.

En ambiente de trabajo de python.

#+begin_src shell
pip install aqtinstall
#+end_src

Listar las versiones habilitadas.

Para linux para

#+begin_src shell
aqt list-qt linux desktod
#+end_src

En caso de windows.

#+begin_src shell
aqt list-qt windows desktop
#+end_src

En macos

#+begin_src shell
aqt list-qt mac desktop
#+end_src

Ejecutar lo siguiente, en caso de linux y seleccionar la version 6.2.0.

#+begin_src shell
aqt install-qt linux desktop 6.2.0
aqt install-qt linux desktop 6.2.0 gcc_64 -m qtmultimedia qtconnectivity qtdatavis3d qtimageformats qtcharts qt5compat qt3d debug_info qtsensors qtserialbus qtserialport qtshadertools qtvirtualkeyboard qtwaylandcompositor qtwebchannel qtwebengine qtwebsockets qtwebview
#+end_src

Esto descargar� el software en un directorio accesible.

M�s detalle ver [[https://aqtinstall.readthedocs.io/_/downloads/en/latest/pdf/][manual]]

Luego, a�adir a *PATH* la ruta a los binarios de qt.

* Instalar y ejecutar

Para instalar el modulo de "GNSS Charts", teniendo instalado 'poetry'.

#+begin_src shell
poetry install
#+end_src

Se habilita el comando *gnss_charts* para levantar las graficas.

Una vez instalado correctamente, sera necesario correr el programa,
tomando de examples las settings.yaml.

#+begin_src shell
gnss_charts examples/settings.yaml 
#+end_src

* M�dulo N gr�ficas con pyqtgraph

Se define un StationRingBuffer que permita gestionar los datos de cada
estaci�n y cada canal (eje)

Se grafican por fila cada estaci�n.

Se realizan varios scripts para probar de manera progresiva las caracter�sticas.

Por ejemplo, corriento *main_window_n_joinrb* se podra visualizar de
manera controlada N estaciones.

* M�dulo control list

Archivo de prueba:

*control_widget.py*

Car�cter�sticas:

- Cargar lista de elementos
- Mover arriba/abajo los elementos
- Guardar estado
- Retroceder (ctrl z)
- Devolverse (ctrl u)
- A�adir

* M�dulo control tree

Similar al caso de una lista de elementos, mantiene listas de
elementos agrupados. Para eso se prepara un diccionario con elementos,
cada llave principal define un grupo y sus elementos los elementos de lista.


Car�cter�sticas:

- Cargar grupos y lista de elementos
- Mover arriba/abajo los elementos, por grupo y lista
- Guardar estado
- Retroceder (ctrl z)
- Devolverse (ctrl u)
- A�adir grupo y elemento de lista.

* N gr�ficas y control

Se unen los m�dulos que permiten mostrar N estaciones y la ventana de
control.

La ventana de control deber�a, frente a cada acci�n, enviar mediante
una cola los datos accionar�n sobre la serie de gr�ficos.


* Iconos para cada GUI

Se deben preparar los iconos identificatorios para cada GUI.


Para los diagramas UML se neceistara 'Version 1.2022.6'

#+begin_src plantuml :file my-diagram.png
!theme spacelab
title Authentication Sequence

Alice->Bob: Authentication Request
note right of Bob: Bob thinks about it
Bob->Alice: Authentication Response
#+end_src

#+RESULTS:
[[file:my-diagram.png]]




#+begin_src shell
mkdir -p images
#+end_src

#+RESULTS:

* Diagramas UML

** Data

La presentacion final de la lectura de datosy su visualicaci�n en
tiempo real dependera directamente de la fuente. Sera necesario
conocer la estructura del dato que genera esta.

Es decir, para el caso presente, en que la fuente es una estacion
GNSS, cada segundo genera un dato que al menos contiene tres canales y
el error asociado.

Cada dato es almacenado en una base de datos de manera secuencial,
cada estaci�n tiene asociada una tabla espec�fica y se almacena en la
medida que se recibe.

Hasta aqu� lo controla 'collector'. La data queda disponible pocos
momentos despues de ser recibida.

** Recepci�n de datos

*** Backend

La ejecuci�n de esta tarea consiste en controlar la lectura a la base
de datos, nos serviremos de la clas 'Rethink_DBS' del m�dulo
'data_rdb' para conectarnos y obtener datos a tramos.

Estas clases /enum/ se utilizan para controlar el flujo de la operaci�n.

Se define /enum/ para controlar conexi�n a rethinkdb.

#+begin_src plantuml  :file ./images/connect_rdb.png
enum DBStep {
	create : int
	connect : int
	collect : int
}
#+end_src

#+RESULTS:
[[file:./images/connect_rdb.png]]

Y otra /enum/ para controlar el env�o al frontend.

#+begin_src plantuml  :file ./images/enum_to_front.png
enum DBSend {
	create : int
	connect : int
	send : int
}
#+end_src

#+RESULTS:
[[file:./images/enum_to_front.png]]


Se define adem�s una clase que controle los datos de conexi�n a
rethinkdb. Seleccionando su host, pory y nombre de base de datos.

#+begin_src  plantuml  :file ./images/dbdata.png
class DBData {
   host:str
   port: int
   dbname: str
   -------
   dict(): Dict
}
#+end_src

#+RESULTS:
[[file:./images/dbdata.png]]

Se define la clase *WorkerReader* que controlara la lectura, c�lculo y
env�o  de los datos  a /frontend/. Adem�s dispone de un m�todo de
clase para acceder a la metadata de las estaciones.


Esta clase se encarga de la tarea de consultar la base de dato que
contiene los datos que se est�n recibiendo en tiempo real, hace el
c�lculo correspondiente del desplazamiento y una estad�stica del tramo
consultado. 

#+begin_src  plantuml  :file ./images/worker_reader.png
class WorkerReader {
	stations: Dict
	dbdata: DBData
	queue_write: queue
	delta_time: int
    ---
	get_state()
	set_state(control,db_insta, dataset, di)
    classmethod load_stations()
	async obtain_data(control, db_Insta, dataset, di)
	task()
}
#+end_src

#+RESULTS:
[[file:./images/worker_reader.png]]

Aqu� la tarea mas importante es la corrutiva 'obtain_data' que es un
ciclo as�ncrono contrlado por /TaskLoop/, que se activa al inicializar
/task/ dentro de un contexto especifico.


*** Frontend

Ahora, se definen la /dataclass/ asociada a un *QObject* que declara
una serie de /signals/ asociadas a un objeto en particular.

#+begin_src   plantuml  :file ./images/worker_signals.png
class WorkerSignals {
finished
log: Signal()
error: Signal(MessageLog)
result: Signal(MSGException)
sation: Signal(StationAction)
default:Signal(StationAction)
----
dic(); Dict
}
#+end_src

#+RESULTS:
[[file:./images/dbdata.png]]

Estas se�ales se deben especificar y asociar a una funci�n que se
activar� una vez cada hilo levante un *Worker* y le asigne la tarea de
procesar la informacion. 

Este es un objeto construido para operar dentro de un /thread/ o hilo
de activacion, heredando de *QRunnable*  y *QObject*.

#+begin_src   plantuml  :file ./images/worker.png
class Worker {
queue_reader: queue
window: gui
signals: WorkerSignals
----
run(): Slot
}
#+end_src

#+RESULTS:
[[file:./images/worker.png]]

El m�todo run tiene la misi�n de recibir datos y aplicar el criterio
de asignacion segun la acci�n definida en ese objeto.

** QueueSet

QueueSet es un m�dulo para la gesti�n de colas. Estas permiten dirigir
elementos seg�n la agrupaci�n asignada. En el caso particular env�a
datos de cada estaci�n a la GUI que corresponda en que se vaya a
visualizar su traza.

Para iniciar, Se debe entregar una queue que reciba los datos de las
estaciones y los distribuya seg�n los grupos asignados.

Estos elementos ser�n de utilidad ya que operan como l�neas de
comunicacion entre componentes que operan diferentes tareas y
necesitan enviar o recibir elementos.

#+begin_src plantuml :file ./images/queue_pair.png
class "QueuePair" as QP {
name : str
optional **queue** : queue
}

class "QueueSet" as QS {
source_queue: queue
groups: Dict[str,str]
optional **queues** : Dict[str, QueuePair]
--
add(QueuePair)
get(name:str)
send(key:str,value)
put(data)
read(key)
set_timeout(n)
unprotect()
group(name)
}


QP o.. QS
#+end_src

#+RESULTS:
[[file:./images/queue_pair.png]]

Una vez creada la instancia de *QueueSet* ser� posible agregar una
*QueuePair* por grupo. Gestionando desde entonces la recepcion o envio
de datos enrutados por grupo.

** RingBuffer

En 'station_ring_buffer' se definen las clases que hereden de
'StationRingBufffer', clase que se define en 'ring_buffer.py' y controla
los datos que vienen entrado y descarta aquellos que estan fuera del
rango de tiempo definido.

Se define, en primera instancia la clase base 'StationRingBufffer'

#+begin_src plantuml  :file ./images/ringbuffer.png
class StationRingBufffer{
seconds:int
ringbuffer:List[DataItem]
name:str
time_field:str
keys:List[str]
delta:int

length
media
mu
sigma
limites
---
remove(item)
refresh(first:datetime)
add(data:DataItem)
get()
clear()
getdata()
}
#+end_src

#+RESULTS:
[[file:./images/ringbuffer.png]]


Luego, cada clase particular 'StandardRingBuffer' implementara el
m�todo 'update' para conectar con los 'widget' que grafican.

* Plot stations widget.

En el archivo *plot_station.py* se implementa la clase que gestiona
los /widget/ para cada canal del sensor se crea un 'PlotWidget' que
viene de 'pyqtgraph', ademas un 'GraphicsLayoutWidget' sobre el que
poner cada plot.

Se define el m�todo 'setData' t�pico que requieren los widget de
pyqtgraph y se actualizan con nuevos datos los tres canales.

#+begin_src plantuml  :file ./images/plot_stations.png
class PlotStation {
seconds:int
p1: PlotWidget
p2: PlotWidget
p3: PlotWidget
layout: GraphicsLayoutWidget
curve_width: int

map_axis

pos()
update_x_axis(now:datetime)
setData(data)
enableAutoRange(axis,auto:bool)
update_seconds(n:int)
}
#+end_src

#+RESULTS:
[[file:./images/plot_stations.png]]


* Station Layour for PlotStation

Se define el layout que permitira asignar posicionalmente cada
PlotItem widget dentro de una grilla especializada para la
visualizacion de gr�ficas.


#+begin_src plantuml  :file ./images/station_layout.png
class StationLayoutWidget{
plots
seconds:int
plot_stations:PlotStation
linked_plot:PlotStation
elements:int
map_axis

set_time(seconds)
start(name,seconds)
update_x_axis(now)
setData(data)
get_grame()
update_seconds(n)
set_update(update:Callable)
set_name(name, angle, **conf)
set_plots(plot_station)
create_chart(name, title_names, 
		axis_time, parent_layout, plot_before)
set_elements(n)
updateViewBox(extra_height)
updateViews()
}
#+end_src

* Union de RingBuffer y PlotStation

Con el fin de administrar correctamente los datos que se visualizaran
en PlotStation, se asigna a cada widget de este tipo un
ringbuffer. Para esto se define una clase *ItemDataJoinRingBufferPlot*
que realice la tarea conjunta. 

La clase *PlotStation* se admnitrara a traves de *StationLayoutWidget*

#+begin_src plantuml  :file ./images/join_rb_plot.png
class ItemDataJoinRingBufferPlot{
name:str
position:int
ringbuffer: StationRingBufffer
plot: StationLayoutWidget
width:int
extra_height

limits

update_seconds(n)
create_chart()
get_frame()
update()
add(item)
}
#+end_src

* TODO Interfaz de control

La interfaz de control permite visualizar la agrupaci�n de sensores
por ventanda, adem�s de administrar el estado posicional de cada
elemento. Con acciones como crear, guardar, deshacer o
rehacer. Destacar� por mejorar la experiencia de usuario en la
administraci�n de las visualizaciones.


** Ventana principal.

En *mwnc_realtime.py* se presenta la c�lmine del desarrollo de la
interfaz de visualizaciones de m�ltiples gr�ficas.

Integra: 

- gr�ficas en interfaz
- tiempo real para las graficas
- agrupaci�n de sensores en diversas ventanas.

