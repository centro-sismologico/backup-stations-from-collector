"""
Create un panel de control con los iconos de acciones
conectadas a funciones en especifico
"""


"""
Cargar iconos en path
"""

from pathlib import Path
from PySide6.QtGui import QIcon
from PySide6.QtGui import QStandardItemModel, QStandardItem
import csv
import sys
from PySide6.QtWidgets import (
    QMainWindow, QApplication,
    QLabel, QToolBar, QStatusBar, QListView, QLineEdit, QTreeView,
)
from PySide6.QtGui import QAction, QIcon
from PySide6.QtCore import Qt,QSize,QItemSelectionModel,QModelIndex
from functools import partial
import copy
from PySide6 import QtCore, QtGui
from rich import print
import pickle
from pathlib import Path
from rich import print 

from enum import IntEnum, auto

ACTIONS = Path(__file__).parent / "icons" / "actions.csv"

STATE_PATH = Path(__file__).parent / "states_stations.pydat"


class ChartAction(IntEnum):
    UP = auto()
    DOWN =  auto()
    UNDO = auto()
    REDO = auto () 
    SAVE = auto()
    CREATE = auto()
    DELETE = auto()
    DEFAULT = auto()

class ActionsToolBar(QToolBar):

    def __init__(self, title,
                 control,
                 actions=ACTIONS,
                 lang="es",
                 memory=20,
                 state_path=STATE_PATH):
        super().__init__(title)
        self.state_path = state_path
        self.control = control
        data = self.loadState()
        self.position  = data["position"]
        self.states = data["states"]
        self.memory = memory
        self.buttons = {}
        self.lang = lang
        self.edit = QLineEdit()
        self.addWidget(self.edit)
        self.icons = actions.parent 
        iconset = []
        with actions.open() as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.addAction(row)

    def addAction(self, data):
        filepath = self.icons / f"{data['file']}.svg"
        icon = QIcon(str(filepath))
        button = QAction(icon, data[self.lang].capitalize(), self)
        button.setStatusTip(data[self.lang])
        self.buttons[data["en"]] = button
        super().addAction(button)

    def setAction(self, action, callback):
        button  = self.buttons.get(action.name.lower())
        if button:
            button.triggered.connect(partial(self.controlState, action, callback))

    def showActions(self):
        return list(self.buttons.keys())

    def saveState(self):
        data = {
            "position": self.position, 
            "states": self.states
        }
        databytes = pickle.dumps(data)
        self.state_path.write_bytes(databytes)

    def loadState(self):
        if self.state_path.exists():
            databytes = self.state_path.read_bytes()
            data = pickle.loads(databytes)
            return data
        else:
            return {"position":0, "states":[]}

    def getGroups(self):
        if self.states:
            return self.states[self.position]
        return {}

    def controlState(self, action, callback):
        print(f"Iniciando accion {action}")

        if not self.states:
            state = self.control.getState()
            self.states.append(state)
            self.position = 0

        if action.name.lower() in self.showActions():
            if action == ChartAction.CREATE:
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif action == ChartAction.DELETE:
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1 
            elif action == ChartAction.UP:
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states)  - 1
            elif action == ChartAction.DOWN:
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif action == ChartAction.UNDO:
                callback()
                self.position = max(0, self.position-1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position - 1
            elif action == ChartAction.REDO:
                callback()
                self.position = min(len(self.states)-1, self.position+1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position 
            elif action == ChartAction.SAVE:
                callback()

            while len(self.states) > self.memory:
                """
                control size of buffer
                """
                
                st = self.states.pop(0)
                print("Free", st)


    def setMemory(self, value:int):
        self.memory = value


class GroupsModel(QStandardItemModel):

    def __init__(self, groups={}):
        super().__init__()
        self.updateGroups(groups)

    def add(self, group, item):
        # get tree
        if group not in self.groups:
            group.append(item)
            group.sort()

    def move(self, parent:QStandardItem, index_a:QModelIndex, index_b:QModelIndex):
        """
        Move only in the same group
        """
        if parent:
            removed = parent.takeRow(index_a.row())
            parent.insertRow(index_b.row(), removed)
        else:
            removed = self.takeItem(index_a.row())
            self.takeRow(index_a.row())
            self.insertRow(index_b.row(), removed)
        return index_b


    def create(self, index, item, selected):
        """
        
        If at parent -> None -> group
        If at parent -> Not None -> item in group
        """
        model = index.model()
        elem = self.itemFromIndex(index)
        parent = None
        if elem:
            parent = elem.parent()
        if item.capitalize() not in self.dict():
            #self.insertRow( index.row() )
            item = QStandardItem(item.capitalize())
            if selected:
                parent_item = index.model().itemFromIndex(index)
                parent = item.parent()
                if not parent:
                    parent_item.appendRow( item)
                else:
                    parent_item.insertRow( index.row(), item)
            else:
                self.insertRow( index.row(), item)

    def up(self, index):
        # get index
        model = index.model()
        item = index.model().itemFromIndex(index)
        parent = item.parent()
        index_b = index
        print("Model up", index)
        if parent:
            if index.row() == 0:
                index_b = index
            else:
                item_b = parent.child(index.row() - 1)
                index_b = item_b.index()
        else:
            new_position = index.row() - 1
            index_b = self.index(max(0, new_position), index.column())
        print("Model UP", parent)
        if index != index_b:
            return parent, self.move(parent, index, index_b)
        return parent, index
        
    def down(self, index):
        # get index
        item = index.model().itemFromIndex(index)
        parent = item.parent()
        index_b = index
        print("Model down", index)

        if parent:
            if index.row() == parent.rowCount() - 1:
                index_b = index
            else:
                item_b = parent.child(index.row() + 1)
                index_b = item_b.index()
        else:
            index_b = self.index(index.row() + 1, index.column())

            if index.row() == self.rowCount() - 1:
                index_b = index

        print("Model DOWN", parent)

        if index != index_b:
            return parent, self.move(parent, index, index_b)
        return parent, index
    # def rowCount(self, index):
    #     """
    #     index: QModelIndex
    #     """
    #     return len(self.countries)

    def delete(self, index):
        self.removeRow(index.row())


    def updateGroups(self, groups):
        self.clear()
        for g, itemset  in groups.items():
            item = QStandardItem(g)
            elem = self.appendRow(item)
            print(item, elem, itemset)
            for e in itemset:
                item.appendRow(QStandardItem(e))

    def dict(self):
        """
        Obtain the tree structure
        """
        data = {}

        for i in range(self.rowCount()):
            index = self.index(i,0)
            self.model_to_dict(data, index)
        return data

    def model_to_dict(self, data, index):    
        v = {}
        for i in range(self.rowCount(index)):
            ix = self.index(i, 0, index)
            self.model_to_dict(v, ix)
        data[index.data()] = v

    

import queue

class ControlWindow(QMainWindow):

    def __init__(self, parent=None, groups={}, queue=queue.Queue()):
        super().__init__(parent)
        self.model = None
        self.selected_item = 0
        self.queue = queue
        if parent:
            self.queue = parent.queue
        toolbar = ActionsToolBar(
            "Acciones sobre grupo",
            control=self)
        toolbar.setIconSize(QSize(16,16))
        self.toolbar = toolbar
        tb_groups = self.toolbar.getGroups()
        if not tb_groups:
            groups = groups
        else:
            groups = tb_groups
        self.view = QTreeView()
        #connect view with countries model
        self.setState(groups)     
        self.setWindowTitle("ToolBar Test")

        label = QLabel("Hello!")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)

        self.addToolBar(toolbar)
        self.setToolBarActions()
        self.setStatusBar(QStatusBar(self))
        self.setCentralWidget(self.view)
        self.show()

    def setToolBarActions(self):
        custom = {"up", "down", "delete", "create", "save"}
        actions = set(self.toolbar.showActions())
        for item in (actions-custom):
            self.toolbar.setAction(ChartAction.DEFAULT, partial(print, item))
        self.toolbar.setAction(ChartAction.UP, self.up)
        self.toolbar.setAction(ChartAction.DOWN, self.down)
        self.toolbar.setAction(ChartAction.DELETE, self.delete)
        self.toolbar.setAction(ChartAction.CREATE, self.create)
        self.toolbar.setAction(ChartAction.SAVE, self.save)
        # create on enter
        create = partial(self.toolbar.controlState, ChartAction.CREATE, self.create)
        self.toolbar.edit.returnPressed.connect(create)


    def up(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            parent, idx = self.model.up(index)
            self.view.selectionModel().select(idx,QItemSelectionModel.ClearAndSelect)
        """
        send by que up command
        """
        print("UP Parent....", parent)
        if parent:
            """
            parent is itemmodel
            """
            command = {
                "action":  ChartAction.UP,
                "position": index.row(),
                "parent": parent.text()
            }
            self.queue.put(command)

    def down(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        parent = None
        for index in reversed(indexes):
            parent, idx = self.model.down(index)
            self.view.selectionModel().select(idx, QItemSelectionModel.ClearAndSelect)
        """
        send by que up command
        """
        print("DOWN Parent....", parent)

        if parent:
            """
            parent is itemmodel
            """
            command = {
                "action":  ChartAction.DOWN,
                "position": index.row(),
                "parent": parent.text()
            }
            self.queue.put(command)

    def save(self):
        self.toolbar.saveState()

    def delete(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            self.model.delete(index)

    def create(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        selected = False
        if indexes:
            index = indexes[-1]
            selected = True

        else:
            index = self.model.createIndex(0,0)

        self.view.selectionModel().select(index, QItemSelectionModel.ClearAndSelect)
            
        # obtener texto de input
        text = self.toolbar.edit.text()
        if text:
            print(f"Loading text-> {text}")
            self.model.create(index, text, selected)
            # limpiar texto de input
            text = self.toolbar.edit.clear()



    def onMyToolBarButtonClick(self, s):
        print("click", s)

    def setState(self, state):
        self.groups = state
        self.updateWidget(self.groups)

    def getState(self):
        state =  self.model.dict()
        return state

    def updateWidget(self, groups):
        # self.model = CountriesModel(countries)
        # self.view.setModel(self.model)
        if not self.model:
            self.model = GroupsModel(groups)
            self.view.setModel(self.model)
        else:
            self.model.updateGroups(groups)

