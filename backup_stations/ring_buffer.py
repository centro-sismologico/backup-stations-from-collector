from typing import List, Text
from dataclasses import dataclass, field
import math
import numpy as np
import numpy.typing as npt
from networktools.geo import GeoData, TimeData, NEUData, DataPoint, DataItem
from vispy.scene.visuals import Line
from vispy.scene import SceneCanvas 
from vispy import app, scene
from rich import print

from typing import Tuple, Dict,Any, Optional
from datetime import datetime, timedelta, timezone
from bisect import bisect
import pytz
MINUTES = 10


def to_timestamp(elem):
    ts =  elem.get_datetime().timestamp()
    utc_ts = datetime.utcfromtimestamp(ts)
    aware_utc_dt = utc_ts.replace(tzinfo=pytz.utc)
    return ts

@dataclass
class StationRingBuffer:
    """
    Add the line linked to canvas
    Manage colors by line
    """
    #latencies: npt.ArrayLike = field(default_factory=lambda:
    #np.zeros(shape=0))
    seconds: int
    ringbuffer: List[DataItem]
    name: Text 
    time_field: Text
    keys: List[str]
    delta: int = 10

    def remove(self, item:DataItem):
        self.ringbuffer.remove(item)

    def refresh(self, first:datetime=None):
        now = datetime.utcnow()
        if not first:
            first = now - timedelta(seconds=self.seconds + self.delta) 

        first = first.replace(tzinfo=timezone.utc)

        remove = []
        for elem in self.ringbuffer:
            elem_time = elem.get_datetime()
            if elem_time < first:
                remove.append(elem)
            else:
                break
        for elem in remove:
            self.remove(elem)
            
    def add(self,data:DataItem):
        if self.ringbuffer:
            dt = data.get_datetime()
            index = self.length
            flag = True
            for elem in self.ringbuffer[::-1]:
                if elem.get_datetime() < dt:
                    break
                index -= 1
                flag = False
            self.ringbuffer.insert(index, data)
            # for python 3.10
            # index = bisect(
            #     self.ringbuffer,
            #     data,
            #     key=lambda e:e.get_datetime())
            return index, flag
        else:
            self.ringbuffer.append(data)
            return self.length, True

    @property
    def length(self):
        return len(self.ringbuffer)

    def get(self):
        return self.ringbuffer

    @property
    def media(self):
        if self.length > 0:
            dataset = [item.data for item in self.ringbuffer]
            return sum(dataset)/len(dataset)
        return 0

    @property
    def mu(self):
        return self.media

    @property
    def sigma(self):
        return self.desviacion_estandar

    def clear(self):
        self.ringbuffer.clear()
        self.last_value = None

    def getdata(self):


        data = {
            key:[(to_timestamp(elem), getattr(elem, key).value) for elem in self.ringbuffer] 
            for key in self.keys
        }
        return data

    @property
    def limits(self):
        ts = [elem.get_datetime() for elem in self.ringbuffer]
        if ts:
            return (min(ts), max(ts))
        else:
            return (None,None)
        
    def __iter__(self):
        return iter(self.ringbuffer)




@dataclass
class Descriptor:
    name: str
    color: str

    def color_hex(self):
        pass


