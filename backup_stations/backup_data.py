import shutil
import tarfile
import os.path
import functools
import concurrent.futures as cf
import queue
from pytimeparse.timeparse import timeparse
from .queue_set import QueueSet
import math
import functools
import yaml
import concurrent.futures
from multiprocessing import Manager
import typer
import sys
import os
import asyncio
import dataclasses
import time
import signal
import time
import operator
from functools import reduce
from pathlib import Path
from enum import IntEnum, auto
from datetime import datetime, timedelta

from dataclasses import dataclass as std_dataclass, asdict
from typing import Dict, Any, List, Tuple, Callable, Union
from pydantic.dataclasses import dataclass
from pydantic import ValidationError, validator
import copy
import ujson as json
import pytz
import numpy as np
from rethinkdb import RethinkDB

from networktools.messages import MSGException, MessageLog
from networktools.ip import validURL, validPORT
from networktools.library import geojson2json
from networktools.time import get_datetime_di
from networktools.time import timestamp, now
from data_rdb import Rethink_DBS
from tasktools.taskloop import TaskLoop

from rich import print
from dacite import from_dict
from networktools.geo import GeoData, TimeData, NEUData, DataPoint
from .read_data import read_csv, Mode, load_stations
import statistics
import sys
import os
import multiprocessing
from dataclasses import dataclass


@dataclass
class Dates:
    di: datetime
    df: datetime


ORM_URL = os.getenv("ORM_SERVICE_HOST", 'http://10.54.218.196')

rdb = RethinkDB()
STEP = 30


def signal_handle(_signal, frame):
    print("Stopping the Jobs.")
    raise Exception("Breaking the jobs with keyboard")


signal.signal(signal.SIGINT, signal_handle)


class KeyboardInterruptError(Exception):
    pass


class DBStep(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    COLLECT = auto()
    NEXT = auto()


class DBSend(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    SEND = auto()


Dataset = List[Dict[str, Any]]


def aware_dt(dt):
    return pytz.utc.localize(dt)


@dataclass
class DBData:
    host: str
    port: int
    dbname: str

    @validator("host")
    def check_host_is_url(cls, value):
        assert validURL(value), "It's not a valid host url"
        return value

    @validator("port")
    def check_port_is_valid(cls, value):
        assert validPORT(value), "It's not a valid port value 0 to 65534"
        return value

    def dict(self):
        return asdict(self)


class Action(IntEnum):
    CREATE = auto()
    UPDATE = auto()
    LOG = auto()
    DROP = auto()
    NEW_DATES = auto()
    CLOSE = auto()
    END = auto()


@dataclass
class DataGNSS:
    status: Dict[str, Any]
    data: GeoData

    def dict(self):
        return asdict(self)


@dataclass(frozen=True)
class StationAction:
    action: Action
    data: Union[DataGNSS, Dict[str, Any]]

    def dict(self):
        return asdict(self)


def geomean(geodataset: List[GeoData]):
    """
    sum([GeoData])
    """
    new = [g for g in geodataset if g]
    if new:
        g0 = geodataset[0]
        for i, g in enumerate(geodataset):
            if i > 0:
                g0 += g
        return g0


def create(base, code):
    path = base / f"{code}.json"
    writer = path.open("w")
    writer.write("[\n")
    return writer


def tramos(inicio, final, minutos=20):
    lista = []
    tiempo = inicio
    tiempo_last = inicio + timedelta(minutes=minutos)

    while tiempo <= final:
        tiempo_last = tiempo + timedelta(minutes=minutos)
        lista.append((tiempo, tiempo_last))
        tiempo = tiempo_last
    return lista


def make_tarfile(output_filename, source_dir):
    # print("Crearing tar file", source_dir, output_filename)
    with tarfile.open(f"{output_filename}.tar.gz", "w:gz") as tar:
        tar.add(source_dir, arcname=source_dir)


def dt_file(timeinfo):
    year = timeinfo.year
    month = timeinfo.month
    day = timeinfo.day
    hour = timeinfo.hour
    minute = timeinfo.minute
    return f"{year}_{month}_{day}_{hour}_{minute}"


async def count_query(dbname, table, ndi, ndf, session):
    query = rdb.db(dbname)\
               .table(table)\
               .between(
                   ndi,
                   ndf,
                   index="DT_GEN",
                   left_bound="open",
                   right_bound="open")\
               .count().run(session)
    return await query


async def get_query(dbname, table, ndi, ndf, session):
    query = rdb.db(dbname)\
               .table(table)\
               .between(
                   ndi,
                   ndf,
                   index="DT_GEN",
                   left_bound="open",
                   right_bound="open")\
               .nth(0).run(session)
    result = await query
    return result

RDB_LIMIT = 100_000


async def first_value(dbname, table_name, session):
    LIMIT = int(RDB_LIMIT * 0.95)
    endtime = datetime.utcnow()
    endtime = pytz.utc.localize(endtime)
    start_time = endtime - timedelta(seconds=LIMIT)

    CD = 0

    pre_endtime = endtime
    pre_start_time = start_time
    while (cantidad_datos := await count_query(dbname, table_name,
                                               start_time, endtime,
                                               session)) > 0:
        if cantidad_datos == 1:
            break
        CD = cantidad_datos
        pre_endtime = endtime
        pre_start_time = start_time
        endtime = start_time
        start_time -= timedelta(seconds=LIMIT)

    if CD > 0:
        return await get_query(dbname, table_name, pre_start_time, pre_endtime, session)
    else:
        return None


@dataclass
class DateLimits:
    start: datetime
    end: datetime

    def __repr__(self):
        return f"DateLimits({self.start},{self.end})"

    def tramos(self) -> (datetime, datetime):
        return tramos(self.start-timedelta(seconds=5), self.end)


async def big_limits_dates(start_date: datetime) -> List[DateLimits]:
    endtime = datetime.utcnow()
    endtime = pytz.utc.localize(endtime)
    df = start_date + timedelta(seconds=RDB_LIMIT)
    dataset = []
    while df <= endtime:
        dt = DateLimits(start_date, df)
        dataset.append(dt)
        start_date = df
        df = start_date + timedelta(seconds=RDB_LIMIT)
    return dataset

DATA_BACKUP = Path("DATA_BACKUP")


def rmdir(directory: Path):
    for item in directory.iterdir():
        if item.is_dir():
            rmdir(item)
        else:
            item.unlink()
    directory.rmdir()


class Worker:
    end_time: datetime
    base: Path = DATA_BACKUP

    def __init__(self,
                 queue_reader,
                 start_time,
                 end_time,
                 wd_queue,
                 periodic=True,
                 *args,
                 **kwargs):
        super().__init__()
        if self.base.exists():
            rmdir(self.base)
            self.base.mkdir()

        self.periodic = periodic
        self.start_time = start_time
        self.end_time = end_time
        self.base.mkdir(parents=True, exist_ok=True)
        self.queue_reader = queue_reader
        self.wd_queue = wd_queue
        self.pid = self.base / "pid.process"
        self.pid.write_text("")

    async def async_run(self, writer_dict, control, **kwargs):
        stasks = kwargs.get("station_tasks", {})
        kwargs["station_tasks"] = stasks
        end = self.end_time

        action = None
        dataset = []
        # for _ in range(self.queue_reader.qsize()):

        def close(table_name):
            make_tarfile(
                f"GNSS_{table_name}_{dt_file(self.start_time)}_{dt_file(self.end_time)}",
                self.base)
            rmdir(self.base)
            self.base.mkdir()

        def do_action(action, dataset):
            end = self.end_time
            if action == Action.CREATE:
                code = dataset["code"]
                stasks[code] = 0
                control[code] = True
                kwargs["last_time"] = self.end_time

            if action in {Action.CLOSE, Action.END}:
                print("Closing data on::::", dataset)
                table_name = dataset
                close(table_name)

            if action == Action.NEW_DATES:
                code, dates = dataset
                print("Update date limits", dates)
                self.start_time = dates.start
                self.end_time = dates.end
                writer = create(self.base, code)
                print("Set code writer", code, writer)
                writer_dict[code] = writer

                # print("Action-msg CREATE", action)
            if action == Action.UPDATE:
                if dataset:
                    for item in dataset:
                        code = item.station
                        writer = writer_dict.get(code)

                        data = json.dumps(item.dict(),
                                          default=str)
                        if writer:
                            if item.dt_gen >= self.end_time:
                                print(item, self.end_time)
                                print(
                                    f"Closing data extraction for {code}")
                                stasks[code] += 1
                                writer.write(data+"\n]")
                                writer.close()
                                writer_dict[code] = None

                            else:
                                writer.write(data+",\n")
                                stasks[code] += 1
                                dt = aware_dt(datetime.utcnow())
            return end
        for _ in range(self.queue_reader.qsize()):
            action, dataset = self.queue_reader.get()
            end = do_action(action, dataset)

            # if action == Action.UPDATE:
            #     print("Action", action)
            #     for data in msg.data:
            #         print("On worker", data)
            # update the frame with all new data

        # obtener todos los k que sean enteros, es decir aun no han
        # terminado y que esten fuera de la desviación estandar en su
        # cantidad de elementos, si la interseccion con lo que ya son
        # booleans es vacio o un 90% de la cantidad total, entonces
        # terminar programa.

        # todas las estaciones que pasan a TRUE para cerrar el ciclo
        # de consultas

        # todas las pendientes, se cuentan todas las NO cerradas y se
        # sacan de las fuera de std
        # deben ser las mismas, pendientes= 0, 1

        return [writer_dict, control], kwargs

    def run(self):
        loop = asyncio.get_event_loop()
        # self.load_stations(self.queue_write)
        writer_dict = {}
        control = {}
        args = [writer_dict, control]
        stations_tasks = {}
        task = TaskLoop(
            self.async_run,
            args,
            {"stations_tasks": stations_tasks,
                "last_time": dt_now()},
            task_name="write_data")
        task.create()
        loop.run_forever()


class WorkerReader:
    """
    Worker for qt app
    obtain the data and send
    """
    on_db: int = 7

    def __init__(self,
                 dbdata: DBData,
                 queue_write,
                 api_url,
                 start_time,
                 delta_time=2,
                 clean=False,
                 on_db=7,
                 *args,
                 **kwargs):
        super().__init__()
        self.on_db = on_db
        self.clean = clean
        self.api_url = api_url
        self.dbdata = dbdata
        self.queue_write = queue_write
        self.args = args
        self.kwargs = kwargs
        self.delta_time = 5
        if kwargs.get("update_stations"):
            self.stations = self.load_stations(self.queue_write,
                                               api_url, send=False)
        # di = rdb.iso8601(get_datetime_di(delta=self.delta_time))
        self.di = start_time
        self.set_state(DBStep.CREATE, None, self.stations, start_time)
        self.time_update = timedelta(seconds=300)
        self.next_update = datetime.utcnow() + self.time_update

    def get_state(self) -> Tuple[Tuple[DBStep, Rethink_DBS, Dataset, datetime], Dict[str, Any]]:
        return (
            self.control,
            self.db_insta,
            self.dataset,
            self.di
        ), self.kwargs

    @ classmethod
    def load_stations(cls, queue, api_url, send=True):
        # keys = {"id", "code", "name", "host", "interface_port",
        #         "protocol_host", "port", "ECEF_X", "ECEF_Y", "ECEF_Z",
        #         "db", "protocol", "active", "server_id", "network_id",
        #         "table_name", "POSITION"}
        # filename = Path(__file__).parent / "csv/station.csv"
        stations = load_stations(api_url)  # read_csv(filename, Mode.GSOF)
        if send:
            for code, station in stations:
                station_copy = {key: value for key, value in
                                station.items() if key != 'process_instance'}
                new_action = (Action.CREATE, station_copy)
                queue.put(new_action)
        dataset = {code: station for code, station in stations}
        cls.stations = dataset
        return dataset

    def set_state(
            self,
            control: DBStep,
            db_insta: Rethink_DBS,
            dataset: Dataset,
            di: datetime, **kwargs):
        self.control = control
        self.db_insta = db_insta
        self.dataset = dataset
        self.di = di
        self.kwargs = kwargs

    async def obtain_data(
            self,
            control,
            db_insta,
            dataset,
            di,
            df,
            start_dates, **kwargs):
        # do wokr
        print(f"Obtain data from {di}", control)
        now = datetime.utcnow()
        if now >= self.next_update:
            try:
                new_dataset = self.load_stations(
                    None, self.api_url, send=False)
                self.next_update = now + self.time_update
                for k, item in dataset.items():
                    item["process_instance"].close()
                dataset = new_dataset
            except Exception as e:
                print(now(), f"Un error al actualizar stations >{e}<")

        delta_time = int(os.environ.get("DELTA_TIME", 10))

        if control == DBStep.CREATE:
            opts = self.dbdata.dict()
            db_insta = Rethink_DBS(**opts)
            control = DBStep.CONNECT

        if control == DBStep.CONNECT:
            fail = False
            dbname = self.dbdata.dbname
            try:
                await asyncio.wait_for(db_insta.async_connect(), timeout=10)
                await db_insta.list_dbs()
                tables = await db_insta.list_tables()
                tables.append("log")
                # obtain the first dates

            except asyncio.TimeoutError as e:
                print(f"Falla al intentar conectar, {e}")
                control = DBStep.CONNECT
                fail = True
                await asyncio.sleep(30)
            if not fail:
                control = DBStep.COLLECT
                print("Control to collecto", control)

        if control == DBStep.COLLECT:
            key = "DT_GEN"
            filter_opt = {'left_bound': 'open', 'index': key}
            df = df if df else get_datetime_di(delta=0)
            # listar las tablas
            dates = {}
            tables = await db_insta.list_tables()
            opts = {"durability": "hard",
                    "return_changes": True}

            for table_name in tables:

                # obtener el primer elemento de la tabla
                # extraer el DT_GEN


                first_date = await first_value(
                    dbname,
                    table_name,
                    db_insta.session)

                if first_date:

                    first_date = first_date.get("DT_GEN")
                    dates[table_name] = first_date

                   # generar una lista de dateday
                    # para extraer por día
                    # por día
                    dt_dataset = await big_limits_dates(first_date)
                    # print("Dt dataset", dt_dataset)
                    code = table_name.split("_")[0]

                    if dt_dataset:
                        # SEND START NEW FILE
                        first = dt_dataset[0]
                        last = dt_dataset[-1]
                        fe_dates = DateLimits(first.start, last.end)
                        new_action = (Action.NEW_DATES, (code, fe_dates))
                        self.queue_write.put(new_action)
                        for dateday in dt_dataset:
                            # print("Dateday", dateday)
                            lista_tramos = dateday.tramos()
                            try:
                                if table_name == "log":
                                    for (ndi, ndf) in lista_tramos[::-1]:
                                        ndi = rdb.iso8601(ndi.isoformat())
                                        ndf = rdb.iso8601(ndf.isoformat())
                                        query = rdb.db(dbname)\
                                            .table(table_name)\
                                            .between(
                                            ndi,
                                            ndf,
                                            index="DT_GEN",
                                            left_bound="open",
                                            right_bound="open")\
                                            .delete(**opts)
                                        await query.run(db_insta.session)
                                else:
                                    # estaciones tipics
                                    for (ndi, ndf) in lista_tramos[::-1]:
                                        ndi = rdb.iso8601(ndi.isoformat())
                                        ndf = rdb.iso8601(ndf.isoformat())
                                        # print(table_name, "-", ndi, "-", ndf)
                                        # for code, station in dataset.items():
                                        # table_name = station.get("table_name")
                                        station = dataset[code]
                                        # get first date
                                        #
                                        cursor = await db_insta.get_data_filter(
                                            table_name,
                                            [ndi, ndf],
                                            filter_opt,
                                            key)
                                        geodataset = []
                                        # print("Len cursor", len(cursor))
                                        for data in cursor:
                                            process_instance = station.get(
                                                "process_instance")
                                            try:
                                                if process_instance:
                                                    if "POSITION_VCV" in data:
                                                        geodata = process_instance.manage_data(
                                                            data)
                                                        geojson = geojson2json(geodata,
                                                                               destiny='db')
                                                        geojson["time"] = {
                                                            "recv": data.get("DT_RECV"),
                                                            "delta": data.get("DELTA_TIME")
                                                        }

                                                        geodataset.append(
                                                            from_dict(
                                                                data_class=GeoData,
                                                                data=geojson
                                                            )
                                                        )
                                                    else:
                                                        now = dt_now()
                                                else:
                                                    await asyncio.sleep(2.5)
                                            except Exception as e:
                                                print("Data error", data)
                                                await asyncio.sleep(2.5)
                                                raise e
                                        # emit data to GUI
                                        info = {
                                            "start": di,
                                            "end": df,
                                        }
                                        # msg = DataGNSS(info, geodataset)
                                        new_action = (Action.UPDATE, geodataset)
                                        self.queue_write.put(new_action)
                                        await asyncio.sleep(10)
                                        if self.clean:
                                            opts = {"durability": "hard",
                                                    "return_changes": True}

                                            query = rdb.db(dbname)\
                                                .table(table_name)\
                                                .between(
                                                ndi,
                                                ndf,
                                                index="DT_GEN",
                                                left_bound="open",
                                                right_bound="open")\
                                                .delete(**opts)
                                            await query.run(db_insta.session)
                            except asyncio.TimeoutError as te:
                                print(f"Exception {te}")
                                control = DBStep.CONNECT
                        print("Closing station->", table_name)
                        end_action = (Action.END, table_name)
                        self.queue_write.put(end_action)

                # TODO: Crear un reporte por archivo creado en WORKER
                await asyncio.sleep(10)
            # # is the start time minus delta-time
            # """
            # Esto debe cambiar a un control más fino de los tramos de
            # fechas a recuperar en la próxima iteración.

            # Cada 1 día activar la recolección del día anterior
            # """

            # di = df
            # df += timedelta(days=1)

            # self.set_start_time(di)

            # dates = DateLimits(di, df)
            # new_action = (Action.NEW_DATES, dates)
            # self.queue_write.put(new_action)
            control = DBStep.NEXT
            await db_insta.close()

        if control == DBStep.COLLECT:
            await asyncio.sleep(delta_time)

        if control == DBStep.NEXT:
            now = dt_now() - timedelta(days=self.on_db)
            if self.dt_test >= now:
                control = DBStep.CREATE
            await asyncio.sleep(delta_time)

            # set state
        return (control, db_insta, dataset, di, df, start_dates), kwargs

    @ property
    def dt_test(self):
        return self.set_start_time + timedelta(days=1)

    def set_start_time(self, di):
        self.start_time = di

    def task(self):
        loop = asyncio.get_event_loop()
        # self.load_stations(self.queue_write)
        (control, db_insta, dataset, di), kwargs = self.get_state()
        start_dates = {}
        args = [control, db_insta, dataset, di, None, start_dates]
        task = TaskLoop(
            self.obtain_data,
            args,
            {},
            task_name="obtain_data")
        task.create()
        loop.run_forever()


app = typer.Typer()


def generate_queue_set(mp_queue, manager, groups):
    queue_set = QueueSet(mp_queue, groups)
    for g in groups:
        m_queue = manager.Queue()
        queue_set.add(g, m_queue)
    queue_set.distribute()
    return queue_set


def generate_groups(
        dataset: Dict[str, Any],
        elements: int) -> Dict[str, str]:
    n = max(math.ceil(len(dataset) / elements), 1)
    groups = np.array_split(list(dataset), n)
    return {str(i): g for i, g in enumerate(groups)}


def create_reader(dbdata, start_time, queue_data, api_url, clean, on_db):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    start_date = get_start_date(start_time)
    start_time = pytz.utc.localize(start_date)
    db_reader = WorkerReader(dbdata, queue_data, api_url, start_time,
                             clean, on_db)
    db_reader.task()


def get_start_date(start_date: str):
    return datetime.utcnow() + timedelta(seconds=timeparse(start_date))


def dt_now():
    start_date = datetime.utcnow()
    start_time = pytz.utc.localize(start_date)
    return start_time


def run_control(queue_data, start_time: str, save_time, wd_queue):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    start_date = get_start_date(start_time)
    start_time = pytz.utc.localize(start_date)
    endtime = start_time + timedelta(days=1)
    db_reader = Worker(queue_data, start_time, endtime, wd_queue)
    db_reader.run()


def cancel(tasks):
    for t in tasks:
        t.cancel()
        t.result()


def run_worker(settings, wd_queue):
    print("Running backup")
    workers = 2
    if not isinstance(settings, Path):
        print("SETTINGS", settings.__dict__, type(settings))
        settings = Path(settings)
    conf = settings.read_text()
    conf_data = yaml.safe_load(conf)

    print("Settings")
    print(conf_data)
    with concurrent.futures.ProcessPoolExecutor(workers) as executor:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        manager = Manager()
        mp_queue = manager.Queue()
        dataset = WorkerReader.load_stations(
            mp_queue, conf_data["source"]["url"])

        tasks = []
        groups = generate_groups(dataset, elements=100)
        dbopts = conf_data.get("dbdata", {})

        dbdata = None
        if dbopts:
            dbdata = DBData(**dbopts)
        else:
            print("No db data on settings.yaml")
        start_time = conf_data["start_time"]

        task = loop.run_in_executor(
            executor,
            functools.partial(
                create_reader,
                dbdata,
                start_time,
                mp_queue,
                conf_data["source"]["url"],
                conf_data["clean"],
                conf_data.get("on_db", 7)))
        tasks.append(task)
        save_time = conf_data["save_time"]

        task = loop.run_in_executor(
            executor,
            functools.partial(
                run_control,
                mp_queue, start_time, save_time, wd_queue))
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            print("General exception")
            cancel(tasks)
            loop.close()
            os.kill(os.getpid(), signal.SIGKILL)
            os.kill(os.getpid(), signal.SIGTERM)
            sys.exit(1)

            raise e

        except KeyboardInterrupt as ki:
            print("Keyboard exceptio")
            cancel(tasks)
            loop.close()
            os.kill(os.getpid(), signal.SIGKILL)
            os.kill(os.getpid(), signal.SIGTERM)
            sys.exit(1)
            raise ki


async def watchdog(mp_queue):
    while mp_queue.empty():
        await asyncio.sleep(1)
    signal = mp_queue.get()

    if signal:
        processes = multiprocessing.active_children()
        for p in processes:
            p.kill()
        print("Procesos", processes)
        print("QUIT")

        os.kill(os.getpid(), signal.SIGKILL)
        os.kill(os.getpid(), signal.SIGTERM)


def mp_watchog(mp_queue):
    print("Running WATCHDOG")
    asyncio.run(watchdog(mp_queue))


@ app.command("backup_data")
def run_backup(settings: Path):
    mp_queue = multiprocessing.Manager().Queue()
    with cf.ProcessPoolExecutor(max_workers=3) as executor:
        loop = asyncio.get_event_loop()
        task2 = loop.run_in_executor(
            executor, functools.partial(mp_watchog, mp_queue))
        task1 = loop.run_in_executor(executor,
                                     functools.partial(run_worker, settings, mp_queue))
        tasks = [task1, task2]
        # tasks = [task2]
        try:
            print(tasks)
            loop.run_until_complete(asyncio.gather(*tasks))
        except Exception as exc:
            raise exc


def main():
    print("Main->")
    app()


if __name__ == "__main__":
    print("Run app")
    app()
