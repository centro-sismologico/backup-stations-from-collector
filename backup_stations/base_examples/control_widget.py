"""
Create un panel de control con los iconos de acciones
conectadas a funciones en especifico
"""


"""
Cargar iconos en path
"""

from pathlib import Path
from PySide6.QtGui import QIcon
import csv
import sys
from PySide6.QtWidgets import (
    QMainWindow, QApplication,
    QLabel, QToolBar, QStatusBar, QListView, QLineEdit
)
from PySide6.QtGui import QAction, QIcon
from PySide6.QtCore import Qt,QSize,QItemSelectionModel,QModelIndex
from functools import partial
import copy
from PySide6 import QtCore, QtGui

ACTIONS = Path(__file__).parent / "icons" / "actions.csv"


class ActionsToolBar(QToolBar):

    def __init__(self, title, control, actions=ACTIONS, lang="es", memory=20):
        super().__init__(title)
        self.states = []
        self.control = control
        self.position = 0
        self.memory = memory
        self.buttons = {}
        self.lang = lang
        self.edit = QLineEdit()
        self.addWidget(self.edit)
        self.icons = actions.parent 
        iconset = []
        with actions.open() as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.addAction(row)

    def addAction(self, data):
        filepath = self.icons / f"{data['file']}.svg"
        icon = QIcon(str(filepath))
        button = QAction(icon, data[self.lang].capitalize(), self)
        button.setStatusTip(data[self.lang])
        self.buttons[data["en"]] = button
        super().addAction(button)

    def setAction(self, name, callback):
        button  = self.buttons.get(name)
        if button:
            button.triggered.connect(partial(self.controlState, name, callback))

    def showActions(self):
        return list(self.buttons.keys())


    def controlState(self, name, callback):
        print(f"Iniciando accion {name}")

        if not self.states:
            state = self.control.getState()
            self.states.append(state)
            self.position = 0

        if name in self.showActions():
            if name == "create":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif name == "delete":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1 
            elif name == "up":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states)  - 1
            elif name == "down":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif name == "undo":
                callback()
                self.position = max(0, self.position-1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position - 1
            elif name == "redo":
                callback()
                self.position = min(len(self.states)-1, self.position+1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position 


            while len(self.states) > self.memory:
                """
                control size of buffer
                """
                
                st = self.states.pop(0)
                print("Free", st)


    def setMemory(self, value:int):
        self.memory = value


class CountriesModel(QtCore.QStringListModel):
    def __init__(self, countries=[]):
        super().__init__()
        self.setStringList(sorted(set(countries)))

    def add(self, country):
        if country not in self.countries:
            self.countries.append(country)
            self.countries.sort()

    def move(self, index_a:QModelIndex, index_b:QModelIndex):
        elem_a = index_a.data()
        elem_b = index_b.data()
        self.setData(index_a, elem_b)
        self.setData(index_b, elem_a)
        return index_b


    def create(self, index, item):
        if item.capitalize() not in self.stringList():
            self.insertRow( index.row() )
            self.setData(index,item.capitalize())

    def up(self, index):
        # get index
        index_b = index
        if index.row()-1 >= 0:
            index_b = self.index(index.row() - 1)
        if index != index_b:
            return self.move(index,index_b)
        return index
        
    def down(self, index):
        # get index
        index_b = self.index(index.row() + 1)
        if index.row() == len(self.stringList()) - 1:
            index_b = index
        if index != index_b:
            return self.move(index,index_b)
        return index
    # def rowCount(self, index):
    #     """
    #     index: QModelIndex
    #     """
    #     return len(self.countries)

    def delete(self, index):
        self.removeRow(index.row())

    def updateList(self, lista):
        self.setStringList(sorted(set(lista)))


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.model = None
        self.selected_item = 0
        self.countries = ["Chile","Argentina","Peru","Ecuador", "Chile"]
        self.view = QListView()
        #connect view with countries model
        self.setState(self.countries)        
        self.setWindowTitle("ToolBar Test")

        label = QLabel("Hello!")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)

        toolbar = ActionsToolBar("Acciones sobre grupo", control=self)
        toolbar.setIconSize(QSize(16,16))
        self.toolbar = toolbar
        self.addToolBar(toolbar)
        self.setToolBarActions()
        self.setStatusBar(QStatusBar(self))
        self.setCentralWidget(self.view)
        self.show()

    def setToolBarActions(self):
        custom = {"up", "down", "delete", "create"}
        actions = set(self.toolbar.showActions())
        for item in (actions-custom):
            self.toolbar.setAction(item, partial(print, item))
        self.toolbar.setAction("up", self.up)
        self.toolbar.setAction("down", self.down)
        self.toolbar.setAction("delete", self.delete)
        self.toolbar.setAction("create", self.create)
        # create on enter
        create = partial(self.toolbar.controlState, "create", self.create)
        self.toolbar.edit.returnPressed.connect(create)


    def up(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            idx = self.model.up(index)
            self.view.selectionModel().select(idx,QItemSelectionModel.ClearAndSelect)
            

    def down(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            idx = self.model.down(index)
            self.view.selectionModel().select(idx, QItemSelectionModel.ClearAndSelect)

    def delete(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            self.model.delete(index)

    def create(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        if indexes:
            index = indexes[-1]
        else:
            index = self.model.createIndex(0,0)
        text = self.toolbar.edit.text()
        self.model.create(index, text)
        text = self.toolbar.edit.clear()


    def onMyToolBarButtonClick(self, s):
        print("click", s)

    def setState(self, state):
        self.countries = state
        self.updateWidget(self.countries)

    def getState(self):
        return copy.deepcopy(self.model.stringList())

    def updateWidget(self, countries):
        # self.model = CountriesModel(countries)
        # self.view.setModel(self.model)
        countries = sorted(set(countries))
        if not self.model:
            self.model = CountriesModel(countries)
            self.view.setModel(self.model)
        else:
            print("Countries new", countries)
            self.model.updateList(countries)


def run_gui():
    print("run gui start")
    app = QApplication(sys.argv)
    gui = MainWindow()
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())



if __name__ == "__main__":
    try:
        run_gui()
    except Exception as e:
        raise e
    except KeyboardInterrupt as ke:
        raise ke
