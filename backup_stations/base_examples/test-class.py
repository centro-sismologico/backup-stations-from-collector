import datetime
from worker import GeoData
from dacite import from_dict

data = {
        'source': 'DataWork',
        'station': 'CLLA',
        'dt_gen': datetime.datetime(2022, 5, 13, 18, 35, 5),
        'timestamp': 1652466905000,
        'data': {
            'N': {
                'value': 0.027653322296530402,
                'error': 0.0018211056196747904,
                'min': 0.02583221667685561,
                'max': 0.029474427916205193
            },
            'E': {
                'value': -0.182677257388547,
                'error': 0.0018211056196747904,
                'min': -0.1844983630082218,
                'max': -0.1808561517688722
            },
            'U': {
                'value': 0.003447209200727135,
                'error': 0.0018211056196747904,
                'min': 0.0016261035810523447,
                'max': -0.1808561517688722
            }
        },
        'time': {
            'recv': datetime.datetime(2022, 5, 13, 18, 35, 5, 441000),
            'delta': 0.441145
        }
    }


lala =from_dict(data_class=GeoData, data=data)
lala2 =from_dict(data_class=GeoData, data=data)

from rich import print
print(lala.dict())

print("Lala + lala2")
lala + lala2
print(lala.dict())
