"""
Create un panel de control con los iconos de acciones
conectadas a funciones en especifico
"""


"""
Cargar iconos en path
"""

from pathlib import Path
from PySide6.QtGui import QIcon
from PySide6.QtGui import QStandardItemModel, QStandardItem
import csv
import sys
from PySide6.QtWidgets import (
    QMainWindow, QApplication,
    QLabel, QToolBar, QStatusBar, QListView, QLineEdit, QTreeView,
)
from PySide6.QtGui import QAction, QIcon
from PySide6.QtCore import Qt,QSize,QItemSelectionModel,QModelIndex
from functools import partial
import copy
from PySide6 import QtCore, QtGui
from rich import print
import pickle
from pathlib import Path

ACTIONS = Path(__file__).parent / "icons" / "actions.csv"

STATE_PATH = Path(__file__).parent / "states.pydat"

class ActionsToolBar(QToolBar):

    def __init__(self, title, 
                 control, 
                 actions=ACTIONS, 
                 lang="es",
                 memory=20,
                 state_path=STATE_PATH):
        super().__init__(title)
        self.state_path = state_path
        self.control = control
        data = self.loadState()
        self.position  = data["position"]
        self.states = data["states"]
        self.memory = memory
        self.buttons = {}
        self.lang = lang
        self.edit = QLineEdit()
        self.addWidget(self.edit)
        self.icons = actions.parent 
        iconset = []
        with actions.open() as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.addAction(row)

    def addAction(self, data):
        filepath = self.icons / f"{data['file']}.svg"
        icon = QIcon(str(filepath))
        button = QAction(icon, data[self.lang].capitalize(), self)
        button.setStatusTip(data[self.lang])
        self.buttons[data["en"]] = button
        super().addAction(button)

    def setAction(self, name, callback):
        button  = self.buttons.get(name)
        if button:
            button.triggered.connect(partial(self.controlState, name, callback))

    def showActions(self):
        return list(self.buttons.keys())

    def saveState(self):
        data = {
            "position": self.position, 
            "states": self.states
        }
        databytes = pickle.dumps(data)
        self.state_path.write_bytes(databytes)

    def loadState(self):
        if self.state_path.exists():
            databytes = self.state_path.read_bytes()
            data = pickle.loads(databytes)
            return data
        else:
            return {"position":0, "states":[]}

    def getGroups(self):
        if self.states:
            return self.states[self.position]
        return {}

    def controlState(self, name, callback):
        print(f"Iniciando accion {name}")

        if not self.states:
            state = self.control.getState()
            self.states.append(state)
            self.position = 0

        if name in self.showActions():
            if name == "create":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif name == "delete":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1 
            elif name == "up":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states)  - 1
            elif name == "down":
                callback()
                state = self.control.getState()
                self.states = [state for state in
                               self.states[:self.position+1]]
                self.states.append(state)
                self.position = len(self.states) - 1
            elif name == "undo":
                callback()
                self.position = max(0, self.position-1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position - 1
            elif name == "redo":
                callback()
                self.position = min(len(self.states)-1, self.position+1)
                state = self.states[self.position]
                self.control.setState(state)
                #self.position = self.position 
            elif name == "save":
                callback()

            while len(self.states) > self.memory:
                """
                control size of buffer
                """
                
                st = self.states.pop(0)
                print("Free", st)


    def setMemory(self, value:int):
        self.memory = value


class GroupsModel(QStandardItemModel):
    def __init__(self, groups={}):
        super().__init__()
        self.updateGroups(groups)

    def add(self, group, item):
        # get tree
        if group not in self.groups:
            group.append(item)
            group.sort()

    def move(self, parent:QStandardItem, index_a:QModelIndex, index_b:QModelIndex):
        """
        Move only in the same group
        """
        print("Parent ->", parent)
        print("Index a", index_a, "Index b", index_b)
        if parent:
            print("In parent->",parent, "Cantidad childs",parent.rowCount())
            # removed = parent.takeChild(index_a.row(),
            #                            index_a.column())
            # print("Child item removed", removed)
            removed = parent.takeRow(index_a.row())
            print("Child item removed", removed)

            parent.insertRow(index_b.row(), removed)
        else:
            removed = self.takeItem(index_a.row())
            self.takeRow(index_a.row())
            self.insertRow(index_b.row(), removed)
        return index_b


    def create(self, index, item, selected):
        """
        
        If at parent -> None -> group
        If at parent -> Not None -> item in group
        """
        model = index.model()
        elem = self.itemFromIndex(index)
        parent = None
        if elem:
            parent = elem.parent()
        if item.capitalize() not in self.dict():
            #self.insertRow( index.row() )
            item = QStandardItem(item.capitalize())
            if selected:
                parent_item = index.model().itemFromIndex(index)
                parent = item.parent()
                if not parent:
                    parent_item.appendRow( item)
                else:
                    parent_item.insertRow( index.row(), item)
            else:
                self.insertRow( index.row(), item)

    def up(self, index):
        # get index
        model = index.model()
        item = index.model().itemFromIndex(index)
        print(model, item, model==item)
        parent = item.parent()
        index_b = index
        if parent:
            if index.row() == 0:
                index_b = index
            else:
                item_b = parent.child(index.row() - 1)
                index_b = item_b.index()
        else:
            new_position = index.row() - 1
            index_b = self.index(max(0, new_position), index.column())
        if index != index_b:
            return self.move(parent, index, index_b)
        return index
        
    def down(self, index):
        # get index
        item = index.model().itemFromIndex(index)
        parent = item.parent()
        index_b = index
        if parent:
            if index.row() == parent.rowCount() - 1:
                index_b = index
            else:
                item_b = parent.child(index.row() + 1)
                index_b = item_b.index()
        else:
            index_b = self.index(index.row() + 1, index.column())

            if index.row() == self.rowCount() - 1:
                index_b = index

        if index != index_b:
            return self.move(parent, index, index_b)
        return index
    # def rowCount(self, index):
    #     """
    #     index: QModelIndex
    #     """
    #     return len(self.countries)

    def delete(self, index):
        self.removeRow(index.row())


    def updateGroups(self, groups):
        self.clear()
        for g, itemset  in groups.items():
            item = QStandardItem(g)
            elem = self.appendRow(item)
            for e in itemset:
                item.appendRow(QStandardItem(e))

    def dict(self):
        """
        Obtain the tree structure
        """
        data = {}

        for i in range(self.rowCount()):
            index = self.index(i,0)
            self.model_to_dict(data, index)
        return data

    def model_to_dict(self, data, index):    
        v = {}
        for i in range(self.rowCount(index)):
            ix = self.index(i, 0, index)
            self.model_to_dict(v, ix)
        data[index.data()] = v

    


class ControlWindow(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.model = None
        self.selected_item = 0

        toolbar = ActionsToolBar("Acciones sobre grupo", control=self)
        toolbar.setIconSize(QSize(16,16))
        self.toolbar = toolbar
        groups = self.toolbar.getGroups()
        if not groups:
            groups = {
                "norte":["ATJN","PFRJ","CPCO"],
                "centro":["CCSN","VALN","QSCO"],
                "sur":["LVLL","CONS","PARC"]
            }
        self.view = QTreeView()
        #connect view with countries model
        self.setState(groups)        
        self.setWindowTitle("ToolBar Test")

        label = QLabel("Hello!")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)

        self.addToolBar(toolbar)
        self.setToolBarActions()
        self.setStatusBar(QStatusBar(self))
        self.setCentralWidget(self.view)
        self.show()

    def setToolBarActions(self):
        custom = {"up", "down", "delete", "create", "save"}
        actions = set(self.toolbar.showActions())
        for item in (actions-custom):
            self.toolbar.setAction(item, partial(print, item))
        self.toolbar.setAction("up", self.up)
        self.toolbar.setAction("down", self.down)
        self.toolbar.setAction("delete", self.delete)
        self.toolbar.setAction("create", self.create)
        self.toolbar.setAction("save", self.save)
        # create on enter
        create = partial(self.toolbar.controlState, "create", self.create)
        self.toolbar.edit.returnPressed.connect(create)


    def up(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            idx = self.model.up(index)
            self.view.selectionModel().select(idx,QItemSelectionModel.ClearAndSelect)
            

    def down(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            idx = self.model.down(index)
            self.view.selectionModel().select(idx, QItemSelectionModel.ClearAndSelect)

    def save(self):
        self.toolbar.saveState()

    def delete(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        for index in reversed(indexes):
            self.model.delete(index)

    def create(self):
        indexes = self.view.selectedIndexes()
        indexes.sort()
        selected = False
        if indexes:
            index = indexes[-1]
            selected = True

        else:
            index = self.model.createIndex(0,0)

        self.view.selectionModel().select(index, QItemSelectionModel.ClearAndSelect)
            
        # obtener texto de input
        text = self.toolbar.edit.text()
        if text:
            print(f"Loading text-> {text}")
            self.model.create(index, text, selected)
            # limpiar texto de input
            text = self.toolbar.edit.clear()



    def onMyToolBarButtonClick(self, s):
        print("click", s)

    def setState(self, state):
        self.groups = state
        self.updateWidget(self.groups)

    def getState(self):
        state =  self.model.dict()
        print("getState->", state)
        return state

    def updateWidget(self, groups):
        # self.model = CountriesModel(countries)
        # self.view.setModel(self.model)
        if not self.model:
            self.model = GroupsModel(groups)
            self.view.setModel(self.model)
        else:
            print("Countries new", groups)
            self.model.updateGroups(groups)


def run_gui():
    print("run gui start")
    app = QApplication(sys.argv)
    gui = ControlWindow()
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())



if __name__ == "__main__":
    try:
        run_gui()
    except Exception as e:
        raise e
    except KeyboardInterrupt as ke:
        raise ke
