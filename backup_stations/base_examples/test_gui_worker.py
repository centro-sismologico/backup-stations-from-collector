import sys
import time
import traceback
import inspect
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot)
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)
from worker import (Worker, DBData, WorkerReader)


import functools
import concurrent.futures
import multiprocessing as mp
from pathlib import Path
from multiprocessing import Manager, Queue
from multiprocessing.managers import BaseManager


class MainWindow(QMainWindow):
    def __init__(self, queue_reader: Queue):
        super().__init__()
        self.queue_reader = queue_reader
        self.counter = 0
        layout = QVBoxLayout()
        self.l = QLabel("Start")
        b = QPushButton("DANGER!")
        layout.addWidget(self.l)
        layout.addWidget(b)
        w =QWidget()
        w.setLayout(layout)
        self.setCentralWidget(w)
        self.show()
        self.threadpool =  QThreadPool()
        self.run_worker_loop()

    def run_worker_loop(self):
        self.timer = QTimer()
        self.timer.setInterval(2500)
        self.timer.timeout.connect(self.start_worker)
        self.timer.start()

    def progress_fn(self, n):
        print(f"{n} done")

    def print_output(self, s):
        print("Recibido desde otro proceso")
        print(s.data.dict().keys())

    def print_station(self, s):
        print(s)

    def print_log(self, s):
        print("Log", id(s))

    def print_error(self, s):
        print(s)

    def thread_complete(self):
        print("Thread complete")


    def start_worker(self):
        try:
            worker_data = Worker(self.queue_reader, [], {})
            worker_data.signals.result.connect(
                self.print_output)
            worker_data.signals.station.connect(
                self.print_station)
            worker_data.signals.log.connect(self.print_log)
            worker_data.signals.error.connect(
                self.print_error)
            worker_data.signals.finished.connect(
                self.thread_complete)
            worker_data.signals.default.connect(
                self.print_log)
            self.threadpool.start(worker_data)
        except Exception as ex:
            raise ex



    def recurring_timer(self):
        self.counter += 1
        self.l.setText(f"Counter {self.counter}")

import asyncio
from qasync import QEventLoop, QThreadExecutor

AEventLoop = type(asyncio.get_event_loop())


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


def run_gui(queue_reader):
    # DBusQtMainLoop(set_as_default=True)
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)

    gui = MainWindow(queue_reader)
    #gui.showMaximized()
    print("GUI", gui)
    sys.exit(app.exec())

class KeyboardInterruptError(Exception): pass
import asyncio


def create_reader(dbdata, queue_write):
    db_reader = WorkerReader(dbdata, queue_write)
    print("Stations")
    print(db_reader.stations.keys())
    db_reader.task()

if __name__ == "__main__":
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        queue_reader = manager.Queue()

        dbdata = DBData(
            "10.54.218.39",
            28015,
            "collector")

        task = loop.run_in_executor(
            executor,
            functools.partial(
                create_reader,
                dbdata,
                queue_reader))

        tasks.append(task)

        task = loop.run_in_executor(
            executor,
            functools.partial(run_gui, queue_reader))
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyBoardInterrupt:
            raise KeyboardInterruptError()
