# -*- coding: utf-8 -*-
import sys
import asyncio
from typing import Dict
from data import Station
from multi_station_layout_widget import MultiStationLayoutWidget

import pyqtgraph as pg
import functools
import concurrent.futures
import multiprocessing as mp
from multiprocessing import Manager, Queue
from datetime import datetime

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QListWidget
)
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot
)
from PySide6.QtGui import QFont,QGuiApplication

from qasync import QEventLoop, QThreadExecutor

# test imports
from datetime import datetime
from station_layout_widget import StationLayoutWidget
from station_ring_buffer import RingBuffer as RingBufferTest 
from utils import TimeAxisItem, timestamp
from functools import partial
from plot_station import PlotStation
import random
from join_rb_plot import JoinRingBufferPlot
from control_group import ControlWindow, ChartAction
import queue
import numpy as np
from typing import List, Any, Type, Dict, TypeVar
from rich import print

AEventLoop = type(asyncio.get_event_loop())

class QEventLoopPlus(QEventLoop, AEventLoop):
    pass

def create_ring_buffer(seconds):
    data = dict(
        seconds=seconds,
        name="test",
        time_field="dt_gen",
        ringbuffer=[],
        keys=["N","E","U"]
    )
    rb = RingBufferTest(**data)
    return rb

class SimpleMultiplotLayoutWidget(pg.GraphicsLayoutWidget):
    joins:List[JoinRingBufferPlot] = []
    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()
        try:
            for join in self.joins:
                join.update()
        except Exception as e:
            print("Error update chart", datetime.utcnow(), e)
            raise e
    # def set_update(self, update):
    #     self.update = update

    def add_join(self, joinrb):
        self.joins.append(joinrb) 


class MainWindow(QMainWindow):
    groups:Dict[str, MultiStationLayoutWidget]
    stations:Dict[str, Station]

    def __init__(self, groups=[], stations={}, geom=None, queue=queue):
        super().__init__()
        if not groups:
            groups.append("MAIN")
        self.stations = stations
        # queue  lista de comunicación
        self.queue = queue

        parent_layout = QVBoxLayout()
        w = QWidget()
        main_layout = SimpleMultiplotLayoutWidget(size=(500,100))
        #main_layout.useOpenGL()

        self.jrbp = self.create_station_element(main_layout, groups)
        #win.start(60)
        #self.main_win = self.create_charts(w, groups)
        self.main_win = main_layout
        parent_layout.addWidget(self.main_win)
        w.setLayout(parent_layout)
        self.setCentralWidget(w)
        # the timer could activate on the active window?
        self.set_timer(self.main_win)
        self.check_commands()
        # neew activate with a button
        self.showMaximized()
        #self.control_window(self.jrbp)

    def control_window(self, groups):
        control = ControlWindow(self, groups)
        control.show()

    def create_station_element(self, main_layout, groups):
        #names = [f"STA{i:02d}" for i in range(45)]
        jrbp_dict = {g:{} for g in groups}
        plot_before = None
        for g, names in groups.items():
            for position, name in enumerate(names):
                station_layout = StationLayoutWidget()
                station_layout.setContentsMargins(0, 0, 0, 0)
                title_names = False
                axis_time = False
                if position == 0:
                    title_names =  True
                elif position == len(names)-1:
                    axis_time = True
                plot_station = station_layout.create_chart(
                    name,
                    title_names,
                    axis_time,
                    main_layout,
                    plot_before)#create_chart(pg, win)
                # adding item to main layout
                plot_before = plot_station
                main_layout.addItem(station_layout)
                # lalala
                ring_buffer = create_ring_buffer(plot_station.seconds)
                jrbp = JoinRingBufferPlot(
                    name,
                    position,
                    ring_buffer,
                    station_layout)
                jrbp_dict[g][name] = jrbp
                main_layout.add_join(jrbp)
                main_layout.addItem(station_layout, row=position, col=0)
        #jrbp.create_chart()
        return {"group1":jrbp_dict}


    def create_charts(self, parent, groups):
        """
        TODO: Check if parent is for all...
        """
        print("Creating charts")
        for g in groups:
            print(g,self.stations)
            stations_group = {code:station for code, station in
                              self.stations.items() if station.group==g}
            win = MultiStationLayoutWidget(size=(500,100)).start(
                g,
                stations_group)
            print(win)
            self.groups[g] = win
        main_win = self.groups[groups[0]]
        pg.setConfigOptions(antialias=True)
        return main_win

   
    def set_timer(self, win):
        self.timer= QTimer(self)
        self.timer.timeout.connect(win.get_frame)
        self.timer.start(500)


    def check_commands(self):
        self.timer= QTimer(self)
        self.timer.timeout.connect(self.read_queue)
        self.timer.start(1000)


    def read_queue(self):
        try:
            print("Queue size", self.queue.qsize())
            if self.queue.qsize() > 0:
                for i in range(self.queue.qsize()):
                    elem = self.queue.get()
                    print("Action from queue",datetime.utcnow(), elem)
                    self.queue.task_done()
                print("Queue task done...")
        except Exception as e:
            print("Error", datetime.utcnow(),e)
            raise e


def run_control(groups, queue):
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        control = ControlWindow(queue=queue, groups=groups)
        control.show()
        sys.exit(app.exec())

def run_gui(groups, queue):
    try:
        stations = {
            "BASE": Station("BASE", "MAIN", 0)
        }
        # DBusQtMainLoop(set_as_default=True)
        app = QApplication(sys.argv)
        screen = app.primaryScreen()
        geom = screen.availableGeometry()
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        gui = MainWindow(queue=queue, stations=stations, geom=geom, groups=groups)
        #gui.showMaximized()
        sys.exit(app.exec())
    except Exception as e:
        print("Error RUN GUI", datetime.utcnow(),e)
        raise e


T = TypeVar('T')
import math

def generate_groups(dataset:List[str], elements:int)->Dict[int, str]:
    n = math.ceil(len(dataset) / elements)
    groups = np.array_split(dataset, n)
    return {i:g for i,g in enumerate(groups)}

if __name__ == "__main__":
    print("Running GUI")
    workers = 3
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        dataset = [f"STA{i:02d}" for i in range(45)]
        groups = generate_groups(dataset, elements=50)

        print(groups)

        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        queue = manager.Queue()

        task = loop.run_in_executor(
            executor,
            partial(run_control, groups, queue))
        tasks.append(task)

        task = loop.run_in_executor(
            executor,
            partial(run_gui, groups, queue))
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyboardInterrupt as ki:
            raise ki
