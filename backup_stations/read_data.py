import csv
from pathlib import Path 
from networktools.geo import (radius, deg2rad, ecef2llh, llh2ecef)
from data_geo import GeoJSONData
from enum import Enum
import httpx 
import json
import os
from networktools.time import timestamp, now

class Mode(Enum):
    GSOF = 1
    SEED = 2

def get_llh(data):
    data['ecef_x'] = float(data['ecef_x'])
    data['ecef_y'] = float(data['ecef_y'])
    data['ecef_z'] = float(data['ecef_z'])
    x = data['ecef_x']
    y = data['ecef_y']
    z = data['ecef_z']
    (lon, lat, h) = ecef2llh(x, y, z)
    data["position"] = {
        "ecef"  : {
            "x": data['ecef_x'],
            "y": data['ecef_y'],
            "z": data['ecef_z']
        },

        'llh': {'lat': lat, 'lon': lon, 'z': h}
    }


def add_process_instance(station):
    CODE = station['protocol'].upper()
    station_code = station['code']
    kwargs = dict()
    kwargs['code'] = CODE
    kwargs['station'] = station
    kwargs['position'] = station["position"]
    kwargs['log_path'] = str(Path("/tmp/log/db") / 'geo_json_data')
    process_instance = GeoJSONData(**kwargs)
    return process_instance


import time
from rich import print 

def read_csv(filename:Path, mode:Mode, filter_tables=[]):
    dataset = {}
    if filename.exists():
        with open(filename,'r') as f:
            reader = csv.DictReader(f, delimiter=';')
            for row in reader:
                code = row.get("code")
                if mode == Mode.GSOF:
                    row["table_name"] = f"{code}_{mode.name}"
                    get_llh(row)
                    process = add_process_instance(row)
                    row["process_instance"] = process
                if filter_tables:
                    if code in filter_tables:
                        dataset[code] = row
                else:
                    dataset[code] = row                    
    else:
        print("No existe", filename)
    new_dataset = [(key,item) for key, item  in  dataset.items() if item.get("active")=='True']
    new_dataset.sort(key=lambda item:-item[1]["position"]["llh"]["lat"])
    return dict(new_dataset)



SLEEP = 0.0001
ORM_URL = os.getenv("ORM_SERVICE_HOST",'http://10.54.218.196')

def load_stations(url, filter_tables=[]):
    mode = Mode.GSOF
    print(now(),f"URL-> {url}/stations")
    try:
        u = httpx.get(f"{url}/stations")
        while u.status_code != httpx.codes.OK:
            u = httpx.get(f"{url}/stations")
            time.sleep(SLEEP)
        items = json.loads(u.content)
        for item in items:
            code = item.get("code")
            if mode == Mode.GSOF:
                item["table_name"] = f"{code}_{mode.name}"
                process = add_process_instance(item)
                item["process_instance"] = process
        items.sort(key=lambda item:-item["position"]["llh"]["lat"])
        return [(item["code"], item) for item in items]
    except Exception as e:
        print(now(), f"Error al obtener lista de estaciones >{e}<")
        print(url)
        raise e
