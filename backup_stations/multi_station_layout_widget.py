# -*- coding: utf-8 -*-

import pyqtgraph as pg
from typing import Dict
from .data import Station
from .join_rb_plot import JoinRingBufferPlot


class MultiStationLayoutWidget(pg.GraphicsLayoutWidget):
    group: str
    stations: Dict[str, Station]
    charts: Dict[str, JoinRingBufferPlot] = {}
    activated: Dict[str, bool] = {}
    seconds: int = 30
    limit: int = 10
    width: int = 1000
    height: int = 1500

    def start(self, group, stations):
        self.group = group
        self.stations = stations
        self.create_stations_charts()
        pg.setConfigOptions(antialias=True)
        return self

    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()
        for jrbp in self.charts.values():
            jrbp.get_frame()
        #return super().get_frame()

    def create_stations_charts(self):
        """
        The MultiStationLayoutWidget contains N stations <= limit
        For every station create a chart
        """
        for name, station in self.stations.items():
            if self.len([v for v in self.activated.values() if v]) <= self.limit:
                self.add_station(name, station)
            else:
                """message reached limit"""
                print("Se alcanzo límite de estaciones para este canvas")
                break

    def add_station(self, code:str, station:Station):
        """
        For one station,using code and station object instance
        """
        code = code.upper()
        ring_buffer = self.create_ring_buffer()
        plot_layout = self.create_chart(code, station)
        if station.position == 0:
            position = self.length
            station.position = position
        jrbp = JoinRingBufferPlot(
            code, 
            position, 
            ring_buffer,
            plot_layout)
        update_station_callback = jrbp.update
        self.set_update(update_station_callback)
        self.charts[code] = jrbp
        self.activated[code] = True

    def drop_station(self, name):
        name = name.upper()
        station = self.stations.get(name)
        if station:
            try:
                self.removeItem(station)
                del self.stations[name]
                del self.activated[name]
            except KeyError as ke:
                raise ke

    @property
    def length(self):
        return len(self.charts)

    def __iter__(self):
        return iter(self.charts)

    @property
    def size(self):
        return {"n":self.limit,"seconds":self.seconds, "width":self.width, "height":self.height}


    def update(self):
        for jrbp in self.charts.values():
            jrbp.update()
            
    def create_ring_buffer(self):
        data = dict(
            seconds=self.seconds,
            name="test",
            time_field="dt_gen",
            ringbuffer=[],
            keys=["N","E","U"]
        )
        rb = RingBufferTest(**data)
        return rb

    def update_seconds(self, n:int):
        self.seconds =  n
        # set n to every plot
        for elem in self.charts.values():
            elem.update_seconds(n)


    def create_charts(self, stations:Dict[str, Station]):
        """
        Firs take main_win and load the stations charts
        create one station
        """
        # relate the pg generator win the canvas
        for code, station in stations.items():
            self.create_chart(code, station)
