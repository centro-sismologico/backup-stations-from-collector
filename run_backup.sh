#!/bin/bash
EPATH="./DATA_BACKUP/pid.process"
mkdir -p DATA_BACKUP

if [ -f "$EPATH"]; then
    rm $EPATH
fi

touch $EPATH

function check_pid(){
	PID=$$
	echo "PID of this script: $PID"
	while [[ -f $EPATH ]] ; do 
	KILL=$(cat $EPATH)
	echo $PID "-" $KILL
	if [[ "$KILL" == "KILL" ]]; then
		echo "TERMINANDO PROGRAMA BACKUP STATIONS"
		kill -9 $PID
	fi
	sleep 5
	done
}

source ~/.bashrc
workon backup
nohup backup_data examples/backup.yaml &
check_pid

